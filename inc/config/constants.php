<?php
// Root url for the site
define('ROOT_URL', '/');


// Database parameters
// Data source name
define('DSN', 'mysql:host=localhost;dbname=shop_inventory');

// Hostname
define('DB_HOST', 'localhost');

// DB user
define('DB_USER', 'root');

// DB password
define('DB_PASSWORD', 'root');

// DB name
define('DB_NAME', 'shop_inventory');
?>
